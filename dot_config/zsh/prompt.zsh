# zsh/prompt.zsh
export ANA_SOURCE="$ANA_SOURCE -> zsh/prompt.zsh"

# ZSH var, timeout between <Esc> and mode switch update
export KEYTIMEOUT=2

__ana_prompt_vi_insert='%K{blue}%F{white} I %f%k'
__ana_prompt_vi_normal='%K{green}%F{black} N %f%k'

# http://paulgoscicki.com/archives/2012/09/vi-mode-indicator-in-zsh-prompt/
# use vi mode even if EDITOR is emacs
export ANA_PROMPT_VIMODE="${__ana_prompt_vi_insert}"

# ============================================================================
# Show VI mode indicator
# ============================================================================

zle-line-init zle-keymap-select() {
  case ${KEYMAP} in
    (vicmd)       : "${__ana_prompt_vi_normal}" ;;
    (main|viins)  : "${__ana_prompt_vi_insert}" ;;
  esac
  ANA_PROMPT_VIMODE="$_"

  # force redisplay
  zle reset-prompt
  zle -R
}
zle -N zle-line-init
zle -N zle-keymap-select

# ============================================================================
# End of cmd (after pressing <CR> at any point), back to ins mode
# ============================================================================

zle-line-finish() {
  # This will be the prompt for the current line that we're leaving.
  ANA_PROMPT_VIMODE="${__ana_prompt_vi_insert}"
  # Redraw the current line's prompt before advancing to a readline.
  zle reset-prompt
  zle -R
}
zle -N zle-line-finish

# ============================================================================
# On interrupt (<C-c>)
# ============================================================================

TRAPINT() {
  ANA_PROMPT_VIMODE="${__ana_prompt_vi_insert}"
  export ANA_PROMPT_IS_TRAPPED=1
  return $(( 128 + $1 ))
}

# "enable" disables everything else, i.e.
#   bzr cdv cvs darcs hg mtn svk svn tla
# are all disabled
zstyle ':vcs_info:*' enable git

# bzr/svn prompt
# zstyle ':vcs_info:(svn|bzr):*'  branchformat      'r%r'
# zstyle ':vcs_info:(svn|bzr):*'  formats           '(%b)'

# git prompt (and git-svn)
zstyle ':vcs_info:git*' get-revision      true
zstyle ':vcs_info:git*' check-for-changes true
zstyle ':vcs_info:git*' unstagedstr       '*'
zstyle ':vcs_info:git*' stagedstr         '+'
zstyle ':vcs_info:git*' formats           '%F{magenta}(%b%c%u)'
zstyle ':vcs_info:git*' actionformats     '%F{magenta}(%m %F{red}→%F{magenta} %b%c%u)'

# Show untracked files
#
# @see <https://github.com/zsh-users/zsh/blob/master/Misc/vcs_info-examples#L155>
# @see <http://zsh.sourceforge.net/Doc/Release/User-Contributions.html#vcs_005finfo-Quickstart>
# $1 message variable name
# $2 formats/actionformats
+vi-git-untracked() {
  git rev-parse --is-inside-work-tree 2>/dev/null \
    && git status --porcelain | grep -q '??' \
    && hook_com[staged]+='T'
    # This will show the marker if there are any untracked files in repo.
    # If instead you want to show the marker only if there are untracked
    # files in $PWD, use:
    #[[ -n $(git ls-files --others --exclude-standard) ]] ; then
}
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked

# Use custom hook to parse merge message in actionformat
#
# @see <http://zsh.sourceforge.net/Doc/Release/User-Contributions.html#vcs_005finfo-Quickstart>
# $1 message variable name
# $2 formats/actionformats
+vi-git-merge-message() {
  if [[ "${hook_com[action_orig]}" == "merge" ]]; then
    # misc_orig is in the format:
    # bd69f0644eb9aa460da5de9ebf72e2e3c04b30f2 Merge branch 'x' (1 applied)

    # get merge_from branch name
    from=$(printf '%s' "${hook_com[misc]}" | awk '{print $4}' | tr -d "'")

    # modify %m
    hook_com[misc]="${from}"
  fi
}
zstyle ':vcs_info:git*+set-message:*' hooks git-merge-message

__ana_has "vcs_info" && add-zsh-hook "precmd" "vcs_info"

# ============================================================================
# Generic traps
# @see <http://www.dribin.org/dave/blog/archives/2004/01/25/zsh_win_resize/>
# ============================================================================

# @TODO redraw PS1 properly
# TRAPWINCH() {
#   zle && zle -R
# }

# ============================================================================
# components
# ============================================================================

__ana_prompt_left_colors=()
__ana_prompt_left_parts=()
if [[ "$USER" = 'root' ]]
then __ana_prompt_left_colors+=('%F{red}')
else __ana_prompt_left_colors+=('%F{green}')
fi
__ana_prompt_left_parts+=('%n')  # User
__ana_prompt_left_colors+=('%F{blue}')
__ana_prompt_left_parts+=('@')
if [[ -n "$SSH_CONNECTION" ]]
then __ana_prompt_left_colors+=('%F{red}')
else __ana_prompt_left_colors+=('%F{green}')
fi
__ana_prompt_left_parts+=('%m')
__ana_prompt_left_colors+=('%F{blue}')
__ana_prompt_left_parts+=(':')
__ana_prompt_left_colors+=('%F{yellow}')
__ana_prompt_left_parts+=('%~')

__ana_prompt_right_colors=()
__ana_prompt_right_parts=()

# ============================================================================
# precmd - set field values before promptline
# ============================================================================

__ana_prompt::precmd::state() {
  local left_raw="${(%j::)__ana_prompt_left_parts} "
  local left_len=${#left_raw}
  local right_raw=" ${(ej::)__ana_prompt_right_parts}"
  local right_len=${#right_raw}

  local cols
  cols=${COLUMNS:-$(tput cols 2>/dev/null)}
  cols=${cols:-80}

  local left=''
  # colorize
  for (( i = 1; i <= ${#__ana_prompt_left_parts}; i++ )) do
    left="${left}${(%)__ana_prompt_left_colors[i]}${(%)__ana_prompt_left_parts[i]}"
  done
  left="${left} "
  local result="${left}"

  # Right side if has room
  local spaces=$(( $cols - $left_len - $right_len ))
  if (( spaces > 4 )); then
    local right=' '
    # colorize
    for (( i = 1; i <= ${#__ana_prompt_right_parts}; i++ )) do
      right="${right}${(%)__ana_prompt_right_colors[i]}${(e)__ana_prompt_right_parts[i]}"
    done
    result="${result}%F{black}${(l:spaces-1::═:)}%F{blue}${(e)right}%F{blue}"
  fi

  # <C-c> to just output a prompt without the statusline above it
  if (( ${ANA_PROMPT_IS_TRAPPED:-0} == 1 )); then
    export ANA_PROMPT_IS_TRAPPED=0
  else
    print -P "$result"
  fi
}
add-zsh-hook precmd __ana_prompt::precmd::state

# ============================================================================
# prompt main
# ============================================================================

# Actual prompt (single line prompt)
__ana_prompt() {
  PS1=''

  # Time
  #PS1+='%f'

  # VI mode
  PS1+='${ANA_PROMPT_VIMODE}'

  PS1+='%{$reset_color%} '

  # VCS
  PS1+='${vcs_info_msg_0_}'

  # Continuation mode
  PS2="$PS1"
  PS2+='%F{green}.%f '

  # Symbol on PS1 only - NOT on PS2 though
  PS1+='%F{yellow}%#%f %{$reset_color%}'

  # Exit code if non-zero
  RPROMPT='%F{red}%(?..[%?])'
}

__ana_prompt