# zsh/helpers.zsh
export ANA_SOURCE="$ANA_SOURCE -> zsh/helpers.zsh"

# ============================================================================
# Helpers
# ============================================================================

# silently determine existence of executable
# $1 name of bin
__ana_has() { command -v "$1" >/dev/null 2>&1; }

__ana_prefer() {
  __ana_has "$1" || {
    [ -n "$SSH_CLIENT" ] && echo "==> WARN: ${1} not found"
    return 1
  }
}

# pipe into this to indent
__ana_indent() { sed 's/^/    /'; }

# source a file if it exists
# $1 path to file
__ana_source() { [ -f "$1" ] && . "$1"; }

# require root
__ana_requireroot() {
  [ "$(whoami)" = "root" ] && return 0
  __ana_err "Please run as root, these files go into /etc/**/"
  return 1
}

# require executable
# $1 name of bin
__ana_require() {
  __ana_has "$1" && __ana_status "FOUND: ${1}" && return 0
  __ana_err "MISSING: ${1}"
  __ana_err_ "Please install before proceeding."
  return 1
}

__ana_maybemkdir() {
  if [[ ! -d "$1" ]]; then
    __ana_status "Directory ${1} does not exist."
    if [[ -z "$CI" ]]; then
      read "REPLY?          Create? [y/N] "
      [[ ! $REPLY =~ ^[Yy]$ ]] && return 1
    fi
    mkdir -p -- "$1" && __ana_ok "Created ${1}."
  fi
}

__ana_realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

__ana_same() {
  # http://www.tldp.org/LDP/abs/html/exitcodes.html
  local sourcepath="$1"
  local targetpath="$2"
  if [[ -f "$targetpath" ]] || [[ -d "$targetpath" ]]; then
    local resolvedpath=$(__ana_realpath "$targetpath")
    [[ "$resolvedpath" == "$sourcepath" ]] && {
      __ana_status "${targetpath} already linked to ${resolvedpath}."
      return 0
    }
    __ana_warn "${targetpath} not linked to ${resolvedpath}."
    return 10
  fi
  __ana_status "${targetpath} not found."
  return 20
}

__ana_link() {
  if [[ $1 == '-s' ]]; then
    local dosudo=1
    shift
  fi
  local flags="-svf"
  local dest=${@[-1]}
  local srcs; srcs=( ${@:1:-1} )
  if [[ $dest == */ && ! -d $dest ]]; then
    mkdir -p $dest
  elif [[ ! -d ${dest%/*} ]]; then
    mkdir -p ${dest%/*}
  fi
  for lk in ${srcs[@]}; do
    local src
    case $lk in
      /*) src=$lk ;;
      .)  src="$(__topic_path $TOPIC)" ;;
      *)  src="$(__topic_path $TOPIC)/$lk" ;;
    esac
    if [[ -d $src ]]; then
      if [[ $dest != */ && -d $dest && -L $dest ]]; then
        ${dosudo:+sudo} rm -fv $dest
      fi
    fi
    __ana_exec ${dosudo:+sudo} ln $flags $src $dest
  done
}

__ana_load_all() {
  pass 
}

__ensure_repo() {
  local target=$1
  local dest=$2
  if [[ ! -d $dest ]]; then
    if [[ $target =~ "^[^/]+/[^/]+$" ]]; then
      url=https://github.com/$target
    elif [[ $target =~ "^[^/]+$" ]]; then
      url=git@github.com:$USER/$target.git
    fi
    [[ -n ${dest%/*} ]] && mkdir -p ${dest%/*}
    git clone --recursive "$url" "$dest"
  fi
}

__load_repo() {
  __ensure_repo "$1" "$2" && source "$2/$3" || >&2 "Failed to load $1."
}

__is_interactive() { [[ $- == *i* || -n $EMACS ]]; }

__os() {
  case $OSTYPE in
    darwin*) echo macos ;;
    linux*) if   [[ -f /etc/arch-release   ]]; then echo arch
            elif [[ -f /etc/debian_version ]]; then echo debian
            fi ;;
  esac
}

__os_vers() {
  case $OSTYPE in
    darwin*) $(sw_vers -buildVersion) ;;
  esac
}

# ============================================================================
# Loggers
# ============================================================================

__ana_echo() { printf '          %s\033[0;m\n' "$1"; }
__ana_status() { printf '\033[0;34m==>       %s\033[0;m\n' "$1"; }
__ana_status_() { printf '\033[0;34m          %s\033[0;m\n' "$1"; }
__ana_ok() { printf '\033[0;32m==> OK:   %s\033[0;m\n' "$1"; }
__ana_ok_() { printf '\033[0;32m==>       %s\033[0;m\n' "$1"; }
__ana_err() { printf '\033[0;31m==> ERR:  %s\033[0;m\n' "$1" >&2; }
__ana_err_() { printf '\033[0;31m          %s\033[0;m\n' "$1" >&2; }
__ana_warn() { printf '\033[0;33m==> WARN: %s\033[0;m\n' "$1" >&2; }
__ana_warn_() { printf '\033[0;33m          %s\033[0;m\n' "$1" >&2; }
__ana_usage() { printf '\033[0;34m==> USAGE: \033[0;32m%s\033[0;m\n' "$1"; }
__ana_usage_() { printf '\033[0;29m           %s\033[0;m\n' "$1"; }


